![Farinha](farinha.png)

# Farinha

Software livre para disponibilização e acesso a arquivos históricos e culturais diversos, legados/migrados e em criação.

_Farinha é um pó feito pela moagem de grãos crus, raízes, feijões, nozes ou sementes. As farinhas são usadas para fazer muitos alimentos diferentes. A farinha de cereais, particularmente a farinha de trigo, é o principal ingrediente do pão, que é um alimento básico para muitas culturas._ - Wikipédia

![Farinha](BannerFarinha.png)

## Não contém conservantes...

- PHP 8.2
- SQLite
- Bootstrap 5.2
- Docker

## Funções comuns

- Pagina inicial 
- Autenticação de usuário (cadastrar, logar e recuperar senha)
- Gestão de usuários (Verificar, suspender uso etc)
- Quadro de avisos

## Projetos conhecidos feitos com Farinha

- Arquivos da UFBA (https://arquivos.ufba.br)
- Plataforma APEB

## Instalação

### Configurar servidor

`docker compose build`

`docker compose run -u root php-fpm preparar`

### Configurar aplicação

Edite o arquivo `config.php`, na pasta sistema, sem esquecer os dados de servidor SMTP

Altere o host na entrada `server_name` em `nginx.default.conf`

### Ativar

`docker compose up -d`

### Desativar

`docker compose down`

CREATE TABLE IF NOT EXISTS "usuarios" (
        "id"    INTEGER,
        "nome"  TEXT,
        "nomedamae"  TEXT,
        "email" TEXT,
        "celular" TEXT,
        "nacionalidade" TEXT,
        "naturalidade" TEXT,
        "genero" TEXT,
        "cor" TEXT,
        "datanasc" TEXT,
        "escolaridade" TEXT,
        "logradouro" TEXT,
        "complemento" TEXT,
        "bairro" TEXT,
        "cep" TEXT,
        "pais" TEXT,
        "estado" TEXT,
        "cidade" TEXT,
        "termosdeciencia" TEXT,
        "senha" TEXT,
        "docIdentificacao" TEXT,
        "data_cadastro" TEXT,
        "permissoes"    TEXT,
        PRIMARY KEY("id" AUTOINCREMENT)
);

CREATE TABLE IF NOT EXISTS "avisos" (
	"id"	INTEGER,
	"data"	TEXT,
	"autor"	INTEGER,
	"titulo" TEXT,
	"texto"	TEXT,
	"permissoes" TEXT,
	PRIMARY KEY("id" AUTOINCREMENT)
);

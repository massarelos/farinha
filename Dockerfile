FROM php:8.2-fpm-bullseye

COPY --chmod=755 preparar /usr/local/bin/preparar
COPY --from=composer:2 /usr/bin/composer /usr/local/bin/composer

# instala as dependências necessárias
RUN apt-get update && apt-get install -y --no-install-recommends \
    libmemcached-dev \
    zlib1g-dev \
    sqlite3 \
    git \
    unzip \
    && rm -rf /var/lib/apt/lists/*

# instala a extensão Memcached
RUN pecl install memcached \
    && docker-php-ext-enable memcached

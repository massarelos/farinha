<?php

// modelo de arquivo de configuração config.php

// Nome do sistema
$f3->set('FARINHA_NOME', 'Plataforma Farinha');

// URL do sistema
$f3->set('FARINHA_URL', 'https://gitlab.com/massarelos/farinha');

// Diretorio TEMP
$f3->set('FARINHA_TEMP', '/var/tmp/farinha-plataforma/');

// Localização do banco de dados sqlite
$f3->set('FARINHA_SQLITE_DB', '/database/farinha.db');

// Path do Memcached
$f3->set('FARINHA_MEMCACHED', 'memcached=localhost:11211');

// Salt para geração aleatória de senha
$f3->set('FARINHA_BCRYPT_SALT', '');

// Email do sistema
$f3->set('FARINHA_EMAIL_SERVER', 'smtp.migadu.com');
$f3->set('FARINHA_EMAIL_PORT', 465); 
$f3->set('FARINHA_EMAIL_SCHEME', 'ssl'); 
$f3->set('FARINHA_EMAIL_LOGIN', 'exemplo@exemplo.com.br');
$f3->set('FARINHA_EMAIL_PASSWORD', 'senha');

// Nome e email do superadmin
$f3->set('FARINHA_NOME_AJUDA', 'Fulano de Tal');
$f3->set('FARINHA_EMAIL_AJUDA', 'exemplo@exemplo.com.br');

// Cloudflare Turnstile Captcha
$f3->set('FARINHA_TURNSTILE_SITEKEY', '');
$f3->set('FARINHA_TURNSTILE_SECRET', '');

<?php

//obter as permissões que o usuário possui, de acordo com a string obtida
$perms = explode("|", $f3->get('SESSION.permissoes')[0]['permissoes']);

if(in_array('novo', $perms)){

   $f3->set('menu', '

<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
<div class="position-sticky pt-3 sidebar-sticky">
  <ul class="nav flex-column">
    <li class="nav-item">
      <a class="nav-link active" aria-current="page" href="/painel">
        <span data-feather="home" class="align-text-bottom"></span>
        Inicial
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/meu-cadastro">
        <span data-feather="user" class="align-text-bottom"></span>
        Meu Cadastro
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/ajuda">
        <span data-feather="help-circle" class="align-text-bottom"></span>
        Ajuda
      </a>
    </li>
  </ul>
</div>
</nav>
');

} else {

if(in_array('admin', $perms) OR in_array('ALL', $perms)){

   $HTML_admin = '

        <!-- Inicio da Area admin -->
        <li class="nav-item">
          <a class="nav-link active" href="/admin">
            <span data-feather="tool" class="align-text-bottom"></span>
            Administração
          </a>
        </li>
        <hr>
        <!-- Final da Area admin -->

   ';
  } else {
     $HTML_admin = '';
 }


 $f3->set('menu', '

<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
<div class="position-sticky pt-3 sidebar-sticky">
  <ul class="nav flex-column">
    <li class="nav-item">
      <a class="nav-link active" aria-current="page" href="/painel">
        <span data-feather="home" class="align-text-bottom"></span>
        Inicial
      </a>
    </li>
<!--
    <hr>
    <li class="nav-item">
      <a class="nav-link" href="/forum">
        <span data-feather="users" class="align-text-bottom"></span>
        Fórum
      </a>
    </li> -->
    <hr>
' . $HTML_admin . '
    <li class="nav-item">
      <a class="nav-link" href="/meu-cadastro">
        <span data-feather="user" class="align-text-bottom"></span>
        Meu Cadastro
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/ajuda">
        <span data-feather="help-circle" class="align-text-bottom"></span>
        Ajuda
      </a>
    </li>
  </ul>
</div>
</nav>

');
}

<?php

/*

Classe para verificação de Autorização de Acesso (ACL)

  // Exemplo 1: Verifica Autorização do usuario
  $acl = new Autorizacao();
  $acl->verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));
  
  ou (slim way, que eu acho melhor)
  
  // Exemplo 2: Verifica Autorização do usuario
  Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

*/

class Autorizacao {

	public static function verificar($db, $idusuario, $page){

        // Obter string com permissões do usuário
        $perm_usuarios = $db->exec('SELECT permissoes FROM usuarios WHERE id = ?', $idusuario[0]['id']);

        //obter as permissões que o usuário possui, de acordo com a string obtida
        $permissoes = explode("|", $perm_usuarios[0]['permissoes']);

        // verifica se tem permissão especifica OU "|ALL" (que dá acesso a tudo)
        if(!in_array($page, $permissoes) AND !in_array('ALL', $permissoes)){
            die('<script>alert("Acesso não autorizado");window.history.back()</script>');
        }
  }

  public static function possui($db, $idusuario, $testarpermissao){

    // Obter string com permissões do usuário
    $perm_usuarios = $db->exec('SELECT permissoes FROM usuarios WHERE id = ?', $idusuario);

    //obter as permissões que o usuário possui, de acordo com a string obtida
    $perms = explode("|", $perm_usuarios[0]['permissoes']);

    // verifica se tem permissão especifica
    if(in_array($testarpermissao, $perms)){
        return " checked";
    }

    // verifica se tem permissão "ALL"
    if(in_array('ALL', $perms)){
      return " checked disabled";
    }
  }

  public static function termos($db, $idusuario, $testartermos){

    // Obter string com permissões do usuário
    $termos_usuarios = $db->exec('SELECT termosdeciencia FROM usuarios WHERE id = ?', $idusuario);

    //obter as permissões que o usuário possui, de acordo com a string obtida
    if($termos_usuarios[0]['termosdeciencia'] != ''){
       $termos = explode("|", $termos_usuarios[0]['termosdeciencia']);
    }

    // verifica se tem permissão especifica
    if($termos_usuarios[0]['termosdeciencia'] == 'termos1|termos2|termos3|termos4|termos5|termos6'){
        return " checked disabled";
    }

    if($termos_usuarios[0]['termosdeciencia'] != ''){
       if(!str_contains($termos_usuarios[0]['termosdeciencia'], 'termos1|termos2|termos3|termos4|termos5|termos6') AND in_array($testartermos, $termos)){
           return " checked";
       }
   }

  }

}

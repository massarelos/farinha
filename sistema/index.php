<?php

/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

// Composer deps
require_once('vendor/autoload.php');

// Fat-free
$f3 = Base::instance();

// Configs
require('config.php');

// Fat-free vars
$f3->set('TEMP', $f3->get('FARINHA_TEMP'));
$f3->set('CACHE', $f3->get('FARINHA_MEMCACHED'));
$f3->set('db', new \DB\SQL('sqlite:' . $f3->get('FARINHA_SQLITE_DB')));
$f3->set('DEBUG', 3); // 0 = production; 3 = debug mode
$f3->set('TZ', 'America/Bahia');
$f3->set('AUTOLOAD', 'classes/');
$f3->set('UPLOADS', '/uploads');

// Session
new \DB\SQL\Session(
    $f3->get('db'),'session',TRUE,
    function ($session) {
        $f3=\Base::instance();
        if (($ip = $session->ip()) != $f3->get('IP'))
            echo '<script>alert("Mudança de IP ou navegador web requer novo login.");window.location.href = "/login";</script>';
        else
            echo '<script>alert("Mudança de IP ou navegador web requer novo login.");window.location.href = "/login";</script>';

            // The default behaviour destroys the suspicious session.
            return false;
    }
);

// Email
$f3->set('smtp', new SMTP(
    $f3->get('FARINHA_EMAIL_SERVER'),
    $f3->get('FARINHA_EMAIL_PORT'),
    $f3->get('FARINHA_EMAIL_SCHEME'),
    $f3->get('FARINHA_EMAIL_LOGIN'),
    $f3->get('FARINHA_EMAIL_PASSWORD')
));

// Cloudflare
use andkab\Turnstile\Turnstile;

// Menu do painel
if ($f3->get('SESSION.permissoes')) {
    require('painel-menu.php');
}

// ====================================================
// Rotas básicas
// ====================================================

$f3->route(
    'GET /',
    function ($f3) {
        $f3->set('page', 'pub-home');

        // Verifica se está logado TEMPORARIO, RETIRAR EM PRODUCAO
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        echo \Template::instance()->render('templates/home.html');
    }
);

$f3->route(
    'GET /sobre',
    function ($f3) {
        $f3->set('page', 'pub-sobre');

        echo 'Sobre o sistema';
    }
);

$f3->route(
    'GET /cadastrar',
    function ($f3) {
        $f3->set('page', 'pub-cadastrar');

        echo \Template::instance()->render('templates/login.html');
    }
);

$f3->route(
    'POST /proc-cadastrar',
    function ($f3) {
        $f3->set('page', 'pub-cadastrar');

        // ----------------------------------------
        /*
        $turnstile = new Turnstile($f3->get('FARINHA_TURNSTILE_SECRET'));
        $verifyResponse = $turnstile->verify($_POST['cf-turnstile-response'], $_SERVER['REMOTE_ADDR']);

        if ($verifyResponse->isSuccess()) {
            // successfully verified captcha resolving
        */

            // Fazer auditoria do endereço de email usado
            $audit = \Audit::instance();

            // Verifica se o email é válido
            if (!$audit->email($f3->get('POST.inputEmail'), TRUE)) {
                die('<script>alert("O email informado não parece válido! Tente novamente com outro endereço eletrônico.");' .
                    'window.location.href = "/cadastrar";</script>');
            }

            // Verifica se já há usuário com mesmo email
            $f3->set('res_usuario', $f3->get('db')->exec('SELECT id FROM usuarios WHERE email = ?', $f3->get('POST.inputEmail')));
            if (count($f3->get('res_usuario')) > 0) {
                die('<script>alert("Esse email já está em uso por um cadastro anteriormente criado.");' .
                    'window.location.href = "/cadastrar";</script>');
            }

            // Gerar nova senha
            $crypt = \Bcrypt::instance();
            $characters = '#$%&@=!0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 12; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            // permissoes: '|novo' (recém-cadastrado), '' (usuario normal validado) ou 
            // com permissoes ao estilo '|' (usuario validado e com privilegios de acesso)
            $f3->get('db')->exec(
                'INSERT INTO usuarios ( ' .
                    'nome, ' .
                    'email, ' .
                    'data_cadastro, ' .
                    'permissoes, ' .
                    'senha) ' .
                    'VALUES(' .
                    ':nome, ' .
                    ':email, ' .
                    ':data_cadastro, ' .
                    ':permissoes, ' .
                    ':senha)',
                array(
                    ':nome' => $f3->get('POST.inputNome'),
                    ':email' => $f3->get('POST.inputEmail'),
                    ':data_cadastro' => date('d/m/Y H:i'),
                    ':permissoes' => '|novo',
                    ':senha' => $crypt->hash($randomString, $f3->get('FARINHA_BCRYPT_SALT'))
                )
            );

            // Verificar se é o PRIMEIRO usuário da base de dados; se for, dar status de super admin "|ALL"
            $f3->set('res_usuario', $f3->get('db')->exec('SELECT id FROM usuarios;'));
            if (count($f3->get('res_usuario')) == 1) {
                $f3->get('db')->exec(
                    'UPDATE usuarios SET ' .
                        'permissoes = :permissoes ' .
                        'WHERE email = :email',
                    array(
                        ':permissoes' => '|ALL',
                        ':email' => $f3->get('POST.inputEmail'),
                    )
                );
            }

            // Para enviar mensagem por email
            $f3->get('smtp')->set('To', '"' . $f3->get('POST.inputNome') . '" <' . $f3->get('POST.inputEmail') . '>');
            $f3->get('smtp')->set('From', '"' . $f3->get('FARINHA_NOME') . '" <' . $f3->get('FARINHA_EMAIL_LOGIN') . '>');
            $f3->get('smtp')->set('Subject', '[' . $f3->get('FARINHA_NOME') . '] Cadastro efetuado com sucesso');
            $f3->get('smtp')->set('Errors-to', '<' . $f3->get('FARINHA_EMAIL_LOGIN') . '>');
            $f3->get('smtp')->set('content-type', 'text/html;charset=utf-8');
            $f3->set('mensagem', 'Olá' . ' ' . $f3->get('POST.inputNome') . '!' .
                '<p>Seu cadastro foi efetuado na ' . $f3->get('FARINHA_NOME') . '.</p>' .
                '<p>Essa mensagem foi gerada automaticamente para atender à sua demanda de ingresso na Plataforma.</p>' .
                '<p>Sua senha de acesso é: <strong>' . $randomString . '</strong></p>' .
                '<p>Atenciosamente,</p>' .
                '<p>' . $f3->get('FARINHA_NOME') . '<br><a href="' . $f3->get('FARINHA_URL') . '">' . $f3->get('FARINHA_URL') . '</a></p>');
            $f3->set('sucessoemail', $f3->get('smtp')->send($f3->get('mensagem')));

            if ($f3->get('sucessoemail') == true) {
                $f3->set('emailerro', 'ok');
            } else {
                $f3->set('emailerro', 'falhaenvioemail');
            }

            echo '<script>alert("Cadastro efetuado com sucesso! Verifique o seu email.");window.location.href = "/login";</script>';
        /*
        } elseif ($verifyResponse->hasErrors()) {
            foreach ($verifyResponse->errorCodes as $errorCode) {
                echo $errorCode . '\n';
            }
        } else {
            // unknown reason of failure resolving of captcha
            $f3->reroute('/cadastrar');
        }
        // ----------------------
        */
    }
);

// ====================================================
// Rotas de login
// ====================================================

// Página de Login
$f3->route(
    'GET /login',
    function ($f3) {
        $f3->set('page', 'login');

        echo \Template::instance()->render('templates/login.html');
    }
);

// Verificar dados de login
$f3->route(
    'POST /checklogin',
    function ($f3) {
        $f3->set('page', 'checklogin');

        // ----------------------------------------
        /*
        $turnstile = new Turnstile($f3->get('FARINHA_TURNSTILE_SECRET'));
        $verifyResponse = $turnstile->verify($_POST['cf-turnstile-response'], $_SERVER['REMOTE_ADDR']);

        if ($verifyResponse->isSuccess()) {
            // successfully verified captcha resolving
        */
            // Verifica se está suspenso
            $f3->set('res_usuario', $f3->get('db')->exec('SELECT id,permissoes FROM usuarios WHERE permissoes = "|suspenso" AND email = ?', $f3->get('POST.inputEmail')));
            if (count($f3->get('res_usuario')) > 0) {
                die('<script>alert("Esse email encontra-se suspenso por questões de segurança, por favor, entre em contato conosco.");window.location.href = "/";</script>');
            }

            $f3->set('usuarios', new DB\SQL\Mapper($f3->get('db'), 'usuarios'));
            $crypt = \Bcrypt::instance();
            $f3->set('auth', new \Auth($f3->get('usuarios'), array('id' => 'email', 'pw' => 'senha')));

            if (!$f3->get('SESSION.logged')) {
                if ($f3->get('auth')->login($f3->get('POST.inputEmail'), $crypt->hash($f3->get('POST.inputPassword'), $f3->get('FARINHA_BCRYPT_SALT'))) == false) {
                    $f3->clear('SESSION.logged');
                    session_commit();
                    $f3->reroute('/login');
                } else {
                    $f3->set('SESSION.logged', 'yes');
                    $f3->set('SESSION.id', $f3->get('db')->exec('SELECT id FROM usuarios WHERE email = ?', $f3->get('POST.inputEmail')));
                    $f3->set('SESSION.nome', $f3->get('db')->exec('SELECT nome FROM usuarios WHERE email = ?', $f3->get('POST.inputEmail')));
                    $f3->set('SESSION.email', $f3->get('POST.inputEmail'));
                    $f3->set('SESSION.permissoes', $f3->get('db')->exec('SELECT permissoes FROM usuarios WHERE email = ?', $f3->get('POST.inputEmail')));
                    $f3->reroute('/painel');
                }
            } else {
                $f3->reroute('/painel');
            }
        /*
        } elseif ($verifyResponse->hasErrors()) {
            foreach ($verifyResponse->errorCodes as $errorCode) {
                echo '<script>alert("Por favor, aguarde o sucesso da verificação e tente novamente.");window.location.href = "/login";</script>';
            }
        } else {
            // unknown reason of failure resolving of captcha
            $f3->reroute('/login');
        }
        // ----------------------
        */
    }
);

// Logout
$f3->route(
    'GET /logout',
    function ($f3) {
        $f3->set('page', 'logout');

        $f3->clear('SESSION.logged');
        $f3->clear('SESSION.id');
        $f3->clear('SESSION.nome');
        $f3->clear('SESSION.email');
        $f3->clear('SESSION.permissoes');
        session_destroy();

        $f3->reroute('/login');
    }
);

$f3->route(
    'GET /recuperar-senha',
    function ($f3) {
        $f3->set('page', 'recuperar-senha');

        echo \Template::instance()->render('templates/login.html');
    }
);

$f3->route(
    'POST /proc-recuperar-senha',
    function ($f3) {
        $f3->set('page', 'proc-recuperar-senha');

        $f3->set('res_recover', $f3->get('db')->exec('SELECT id,nome,email FROM usuarios WHERE email = ?', $f3->get('POST.inputEmail')));
        if ($f3->get('db')->count() > 0) {

            // ----------------------------------------
            /*
            $turnstile = new Turnstile($f3->get('FARINHA_TURNSTILE_SECRET'));
            $verifyResponse = $turnstile->verify($_POST['cf-turnstile-response'], $_SERVER['REMOTE_ADDR']);

            if ($verifyResponse->isSuccess()) {
                // successfully verified captcha resolving
            */

                // Gerar nova senha
                $crypt = \Bcrypt::instance();
                $characters = '#$%&@=!0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < 12; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }

                $f3->get('db')->exec(
                    'UPDATE usuarios SET ' .
                        'senha = :senha ' .
                        'WHERE email = :email',
                    array(
                        ':senha' => $crypt->hash($randomString, $f3->get('FARINHA_BCRYPT_SALT')),
                        ':email' => $f3->get('POST.inputEmail'),
                    )
                );

                // Para enviar mensagem por email
                $f3->get('smtp')->set('To', '"' . $f3->get('res_recover')[0]['nome'] . '" <' . $f3->get('res_recover')[0]['email'] . '>');
                $f3->get('smtp')->set('From', '"' . $f3->get('FARINHA_NOME') . '" <' . $f3->get('FARINHA_EMAIL_LOGIN') . '>');
                $f3->get('smtp')->set('Subject', '[' . $f3->get('FARINHA_NOME') . '] Nova senha gerada com sucesso');
                $f3->get('smtp')->set('Errors-to', '<' . $f3->get('FARINHA_EMAIL_LOGIN') . '>');
                $f3->get('smtp')->set('content-type', 'text/html;charset=utf-8');
                $f3->set('mensagem', 'Olá' . ' ' . $f3->get('res_recover')[0]['nome'] . '!' .
                    '<p>Uma nova senha foi requisitada para sua conta na ' . $f3->get('FARINHA_NOME') . '.</p>' .
                    '<p>Essa mensagem foi gerada automaticamente para atender à sua demanda.</p>' .
                    '<p>Sua nova senha é: <strong>' . $randomString . '</strong></p>' .
                    '<p>Atenciosamente,</p>' .
                    '<p>' . $f3->get('FARINHA_NOME') . '<br><a href="' . $f3->get('FARINHA_URL') . '">' . $f3->get('FARINHA_URL') . '</a></p>');
                $f3->set('sucessoemail', $f3->get('smtp')->send($f3->get('mensagem')));

                if ($f3->get('sucessoemail') == true) {
                    $f3->set('emailerro', 'ok');
                } else {
                    $f3->set('emailerro', 'falhaenvioemail');
                }
            } else {
                $f3->set('emailerro', 'noemail');
            }

            echo \Template::instance()->render('templates/login.html');
        /*
        } elseif ($verifyResponse->hasErrors()) {
            foreach ($verifyResponse->errorCodes as $errorCode) {
                echo $errorCode . '\n';
            }
        } else {
            // unknown reason of failure resolving of captcha
            $f3->reroute('/recuperar-senha');
        }
        // ----------------------
        */
    }
);

// ====================================================
// Painel - Inicial
// ====================================================

// Página de Painel
$f3->route(
    'GET /painel',
    function ($f3) {
        $f3->set('page', 'painel');

        // Verifica se está logado
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Contar avisos
        $f3->get('db')->exec('SELECT id FROM avisos');
        $f3->set('contagem', $f3->get('db')->count());

        if ($f3->get('GET.qtd') == '' || $f3->get('GET.qtd') == 0 || $f3->get('GET.de') < 20) {
            $f3->set('GET.de', 0);
        }

        // Listar avisos
        if ($f3->get('GET.q') == '' || $f3->get('GET.q') == null) {
            $f3->set(
                'res_avisos',
                $f3->get('db')->exec(
                    'SELECT usuarios.id,data,titulo,autor,usuarios.nome AS autorNome,avisos.id AS avisoid' .
                        ' FROM avisos INNER JOIN usuarios ON avisos.autor = usuarios.id' .
                        ' WHERE avisos.permissoes != "|suspenso"' .
                        ' ORDER BY avisoid DESC' .
                        ' LIMIT 20' .
                        ' OFFSET ?',
                    $f3->get('GET.de')
                )
            );
        }

        // Se for cadastro novo, vai confinar nos avisos e Meu Cadastro
        echo \Template::instance()->render('templates/painel.html');
    }
);

// ====================================================
// Painel - Avisos
// ====================================================

// Painel - Aviso
$f3->route(
    'GET /aviso',
    function ($f3) {
        $f3->set('page', 'painel');

        // Verifica se está logado
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Aviso
        $f3->set(
            'res_avisos',
            $f3->get('db')->exec(
                'SELECT usuarios.id, avisos.id AS avisosid, avisos.data, avisos.titulo, avisos.autor, avisos.texto,' .
                    'usuarios.nome AS autorNome FROM avisos INNER JOIN usuarios ON usuarios.id = avisos.autor' .
                    ' WHERE avisos.id = ?',
                $f3->get('GET.id')
            )
        );

        echo \Template::instance()->render('templates/painel-aviso.html');
    }
);

// Suspender aviso
$f3->route(
    'GET /admin-avisos-editar-suspender',
    function ($f3) {
        $f3->set('page', 'admin-avisos-editar');

        // Verify if is logged
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Verifica se tem autorização de acesso
        Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

        // Suspender
        $f3->get('db')->exec(
            'UPDATE avisos SET ' .
                'permissoes = :permissoes' .
                ' WHERE id = :id',
            array(
                ':permissoes' => '|suspenso',
                ':id' => $f3->get('GET.id')
            )
        );

        echo '<script>alert("Aviso suspenso com sucesso!");history.back();</script>';
    }
);

// Reativar aviso
$f3->route(
    'GET /admin-avisos-editar-reativar',
    function ($f3) {
        $f3->set('page', 'admin-avisos-editar');

        // Verify if is logged
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Verifica se tem autorização de acesso
        Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

        // Aprovar
        $f3->get('db')->exec(
            'UPDATE avisos SET ' .
                'permissoes = :permissoes' .
                ' WHERE id = :id',
            array(
                ':permissoes' => '',
                ':id' => $f3->get('GET.id')
            )
        );

        echo '<script>alert("Aviso reativado com sucesso!");history.back();</script>';
    }
);

// Avisos :: Escrever
$f3->route(
    'GET /aviso-escrever',
    function ($f3) {
        $f3->set('page', 'admin-aviso-escrever');

        // Verifica se está logado
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Verifica se tem autorização de acesso
        Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

        echo \Template::instance()->render('templates/painel-aviso-escrever.html');
    }
);

$f3->route(
    'POST /proc-aviso-escrever',
    function ($f3) {
        $f3->set('page', 'admin-aviso-escrever');

        // Verifica se está logado
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Verifica se tem autorização de acesso
        Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

        $f3->get('db')->exec(
            'INSERT INTO avisos ( ' .
                'data, ' .
                'autor, ' .
                'titulo, ' .
                'permissoes, ' .
                'texto) ' .
                'VALUES(' .
                ':data, ' .
                ':autor, ' .
                ':titulo, ' .
                ':permissoes, ' .
                ':texto)',
            array(
                ':data' => date('d/m/Y H:i'),
                ':autor' => $f3->get('SESSION.id')[0]['id'],
                ':titulo' => $f3->get('POST.inputTitulo'),
                ':permissoes' => '',
                ':texto' => $f3->get('POST.inputTexto')
            )
        );

        echo '<script>alert("Aviso publicado com sucesso!");history.back();</script>';
    }
);

// Avisos Editar
$f3->route(
    'GET /admin-avisos-editar',
    function ($f3) {
        $f3->set('page', 'admin-avisos-editar');

        // Verify if is logged
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Verifica se tem autorização de acesso
        Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

        $f3->set(
            'res_avisos',
            $f3->get('db')->exec(
                'SELECT titulo,texto FROM avisos WHERE id = ?',
                $f3->get('GET.id')
            )
        );

        echo \Template::instance()->render('templates/admin-avisos-editar.html');
    }
);

$f3->route(
    'POST /proc-admin-avisos-editar',
    function ($f3) {
        $f3->set('page', 'admin-avisos-editar');

        // Verify if is logged
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Verifica se tem autorização de acesso
        Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

        $f3->get('db')->exec(
            'UPDATE avisos SET ' .
                'titulo = :titulo,' .
                'texto = :texto ' .
                'WHERE id = :id',
            array(
                ':titulo' => $f3->get('POST.inputTitulo'),
                ':texto' => $f3->get('POST.inputTexto'),
                ':id' => $f3->get('POST.id'),
            )
        );
        $f3->reroute($f3->get('SERVER.HTTP_REFERER'));


        echo \Template::instance()->render('templates/admin-avisos-editar.html');
    }
);

// ====================================================
// Painel - Administração
// ====================================================

// Página de Administração :: Inicial
$f3->route(
    'GET /admin',
    function ($f3) {
        $f3->set('page', 'admin');

        // Verifica se está logado
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Verifica se tem autorização de acesso
        Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

        echo \Template::instance()->render('templates/painel-admin.html');
    }
);

// Página de Administração :: Avisos
$f3->route(
    'GET /admin-avisos',
    function ($f3) {
        $f3->set('page', 'admin-avisos');

        // Verifica se está logado
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Verifica se tem autorização de acesso
        Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

        // Contar usuários
        $f3->get('db')->exec('SELECT id FROM avisos');
        $f3->set('contagem', $f3->get('db')->count());

        if ($f3->get('GET.qtd') == '' || $f3->get('GET.qtd') == 0 || $f3->get('GET.de') < 20) {
            $f3->set('GET.de', 0);
        }

        // Buscar e listar
        if ($f3->get('GET.q') == '' || $f3->get('GET.q') == null) {

            $f3->set(
                'res_avisos',
                $f3->get('db')->exec(
                    'SELECT id,data,titulo,permissoes FROM avisos' .
                        ' ORDER BY id DESC' .
                        ' LIMIT 20' .
                        ' OFFSET ?',
                    $f3->get('GET.de')
                )
            );
        } else {

            $f3->set(
                'res_avisos',
                $f3->get('db')->exec(
                    'SELECT id,data,titulo,texto,permissoes' .
                        ' FROM avisos' .
                        ' WHERE titulo LIKE :titulo' .
                        ' OR texto LIKE :texto' .
                        ' ORDER BY id DESC' .
                        ' LIMIT 20' .
                        ' OFFSET :de',
                    array(
                        ':titulo' => '%' . $f3->get('GET.q') . '%',
                        ':texto' => '%' . $f3->get('GET.q') . '%',
                        ':de' => $f3->get('GET.de'),
                    )
                )
            );
        }

        echo \Template::instance()->render('templates/admin-avisos.html');
    }
);

// Página de Administração :: Usuários
$f3->route(
    'GET /admin-usuarios',
    function ($f3) {
        $f3->set('page', 'admin-usuarios');

        // Verifica se está logado
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Verifica se tem autorização de acesso
        Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

        // Contar usuários
        $f3->get('db')->exec('SELECT id FROM usuarios WHERE permissoes != "|novo"');
        $f3->set('contagem', $f3->get('db')->count());

        if ($f3->get('GET.qtd') == '' || $f3->get('GET.qtd') == 0 || $f3->get('GET.de') < 20) {
            $f3->set('GET.de', 0);
        }

        // Buscar e listar
        if ($f3->get('GET.q') == '' || $f3->get('GET.q') == null) {

            $f3->set(
                'res_usuarios',
                $f3->get('db')->exec(
                    'SELECT id,nome,email,pais,estado,cidade,data_cadastro,permissoes FROM usuarios WHERE permissoes != "|novo"' .
                        ' LIMIT 20' .
                        ' OFFSET ?',
                    $f3->get('GET.de')
                )
            );
        } else {

            $f3->set(
                'res_usuarios',
                $f3->get('db')->exec(
                    'SELECT id,nome,email,data_cadastro,pais,estado,cidade,permissoes' .
                        ' FROM usuarios' .
                        ' WHERE permissoes != "|novo"' .
                        ' AND ( nome LIKE :nome OR email LIKE :email )' .
                        ' LIMIT 20' .
                        ' OFFSET :de',
                    array(
                        ':nome' => '%' . $f3->get('GET.q') . '%',
                        ':email' => '%' . $f3->get('GET.q') . '%',
                        ':de' => $f3->get('GET.de'),
                    )
                )
            );
        }

        echo \Template::instance()->render('templates/admin-usuarios.html');
    }
);

// Página de Administração :: Editar Usuários
$f3->route(
    'GET /admin-usuarios-editar',
    function ($f3) {
        $f3->set('page', 'admin-usuarios-editar');

        // Verifica se está logado
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Verifica se tem autorização de acesso
        Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

        $f3->set(
            'res_usuarios',
            $f3->get('db')->exec(
                'SELECT * FROM usuarios' .
                    ' WHERE usuarios.id = ?',
                $f3->get('GET.id')
            )
        );

        $f3->set('HTMLpermissoes', '

                <tbody>
                <tr>
                    <td>Acesso ao módulo Administração</td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="admin" value="admin" name="permissoes[]" ' . Autorizacao::possui($f3->get('db'), $f3->get('GET.id'), 'admin') . '>
                            <label class="form-check-label" for="admin">Ver Administração</label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td>Avisos</td>
                        <td>
                            <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="admin-avisos-escrever"
                                value="admin-avisos-escrever" name="permissoes[]" ' . Autorizacao::possui($f3->get('db'), $f3->get('GET.id'), 'admin-avisos-escrever') . '>
                            <label class="form-check-label" for="admin-avisos-escrever">Escrever Avisos</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="admin-avisos-editar"
                                value="admin-avisos-editar" name="permissoes[]" ' . Autorizacao::possui($f3->get('db'), $f3->get('GET.id'), 'admin-avisos-editar') . '>
                            <label class="form-check-label" for="admin-avisos-editar">Gerenciar Avisos</label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td>Usuários</td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="admin-novos-cadastros" value="admin-novos-cadastros"
                                name="permissoes[]" ' . Autorizacao::possui($f3->get('db'), $f3->get('GET.id'), 'admin-novos-cadastros') . '>
                            <label class="form-check-label" for="admin-novos-cadastros">Ver Novos Cadastros</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="admin-novos-cadastros-editar"
                                value="admin-novos-cadastros-editar" name="permissoes[]" ' . Autorizacao::possui($f3->get('db'), $f3->get('GET.id'), 'admin-novos-cadastros-editar') . '>
                            <label class="form-check-label" for="admin-novos-cadastros-editar">Editar Novos Cadastros</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="admin-usuarios" value="admin-usuarios"
                                name="permissoes[]" ' . Autorizacao::possui($f3->get('db'), $f3->get('GET.id'), 'admin-usuarios') . '>
                            <label class="form-check-label" for="admin-usuarios">Ver Usuários</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="admin-usuarios-editar"
                                value="admin-usuarios-editar" name="permissoes[]" ' . Autorizacao::possui($f3->get('db'), $f3->get('GET.id'), 'admin-usuarios-editar') . '>
                            <label class="form-check-label" for="admin-usuarios-editar">Editar Usuários</label>
                        </div>
                    </td>
                </tr>
                
            </tbody>

        ');


        echo \Template::instance()->render('templates/admin-usuarios-editar.html');
    }
);

$f3->route(
    'POST /proc-admin-usuarios-editar',
    function ($f3) {
        $f3->set('page', 'admin-usuarios-editar');

        // Verifica se está logado
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Verifica se tem autorização de acesso
        Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

        $perms = $f3->get('POST.permissoes');
        if ($perms != '') {
            $perm = implode('|', $perms);
        } else {
            $perm = '';
        }

        if($f3->get('POST.idusuario') == '1') {
            die('<script>alert("O superadmmin não pode ser editado.");history.back();</script>');
        }

        $f3->get('db')->exec(
            'UPDATE usuarios SET ' .
                'nome = :nome, ' .
                'nomedamae = :nomedamae, ' .
                'email = :email, ' .
                'celular = :celular, ' .
                'nacionalidade = :nacionalidade, ' .
                'naturalidade = :naturalidade, ' .
                'genero = :genero, ' .
                'cor = :cor, ' .
                'datanasc = :datanasc, ' .
                'escolaridade = :escolaridade, ' .
                'logradouro = :logradouro, ' .
                'complemento = :complemento, ' .
                'bairro = :bairro, ' .
                'cep = :cep, ' .
                'pais = :pais, ' .
                'estado = :estado, ' .
                'cidade = :cidade, ' .
                'permissoes = :permissoes ' .
                'WHERE id = :idend',
            array(
                ':nome' => $f3->get('POST.inputNome'),
                ':nomedamae' => $f3->get('POST.inputNomeDaMae'),
                ':email' => $f3->get('POST.inputEmail'),
                ':celular' => $f3->get('POST.inputCelular'),
                ':nacionalidade' => $f3->get('POST.inputNacionalidade'),
                ':naturalidade' => $f3->get('POST.inputNaturalidade'),
                ':genero' => $f3->get('POST.inputGenero'),
                ':cor' => $f3->get('POST.inputCor'),
                ':datanasc' => $f3->get('POST.inputDataNasc'),
                ':escolaridade' => $f3->get('POST.inputEscolaridade'),
                ':logradouro' => $f3->get('POST.inputLogradouro'),
                ':complemento' => $f3->get('POST.inputComplemento'),
                ':bairro' => $f3->get('POST.inputBairro'),
                ':cep' => $f3->get('POST.inputCEP'),
                ':pais' => $f3->get('POST.inputPais'),
                ':estado' => $f3->get('POST.inputEstado'),
                ':cidade' => $f3->get('POST.inputCidade'),
                ':permissoes' => $perm,
                ':idend' => $f3->get('POST.idusuario')
            )
        );

        echo '<script>alert("As modificações foram salvas!");history.back();</script>';
    }
);

// Suspender cadastro
$f3->route(
    'GET /admin-usuarios-editar-suspender',
    function ($f3) {
        $f3->set('page', 'admin-usuarios-editar');

        // Verify if is logged
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Verifica se tem autorização de acesso
        Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

        // Suspender
        $f3->get('db')->exec(
            'UPDATE usuarios SET ' .
                'permissoes = :permissoes' .
                ' WHERE id = :id',
            array(
                ':permissoes' => '|suspenso',
                ':id' => $f3->get('GET.id')
            )
        );

        echo '<script>alert("Cadastro suspenso com sucesso! Todas as permissões administrativas foram removidas.");history.back();</script>';
    }
);

// Reativar cadastro
$f3->route(
    'GET /admin-usuarios-editar-reativar',
    function ($f3) {
        $f3->set('page', 'admin-usuarios-editar');

        // Verify if is logged
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Verifica se tem autorização de acesso
        Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

        // Aprovar
        $f3->get('db')->exec(
            'UPDATE usuarios SET ' .
                'permissoes = :permissoes' .
                ' WHERE id = :id',
            array(
                ':permissoes' => '',
                ':id' => $f3->get('GET.id')
            )
        );

        echo '<script>alert("Cadastro reativado com sucesso! Reatribua as permissões administrativas, se necessário.");history.back();</script>';
    }
);

// Admin :: Novos Cadastros
$f3->route(
    'GET /admin-novos-cadastros',
    function ($f3) {
        $f3->set('page', 'admin-novos-cadastros');

        // Verifica se está logado
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Verifica se tem autorização de acesso
        Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

        // Contar usuários
        $f3->get('db')->exec('SELECT id FROM usuarios WHERE permissoes = "|novo" AND termosdeciencia = "termos1|termos2|termos3|termos4|termos5|termos6"');
        $f3->set('contagem', $f3->get('db')->count());

        if ($f3->get('GET.qtd') == '' || $f3->get('GET.qtd') == 0 || $f3->get('GET.de') < 20) {
            $f3->set('GET.de', 0);
        }

        // Buscar e listar
        if ($f3->get('GET.q') == '' || $f3->get('GET.q') == null) {

            $f3->set(
                'res_usuarios',
                $f3->get('db')->exec(
                    'SELECT id,nome,email,pais,estado,cidade,data_cadastro,permissoes FROM usuarios' .
                        ' WHERE permissoes = "|novo" AND termosdeciencia = "termos1|termos2|termos3|termos4|termos5|termos6"' .
                        ' LIMIT 20' .
                        ' OFFSET ?',
                    $f3->get('GET.de')
                )
            );
        } else {

            $f3->set(
                'res_usuarios',
                $f3->get('db')->exec(
                    'SELECT id,nome,email,pais,estado,cidade,data_cadastro,permissoes FROM usuarios' .
                        ' WHERE permissoes = "|novo" AND termosdeciencia = "termos1|termos2|termos3|termos4|termos5|termos6"' .
                        ' AND nome LIKE :nome OR email LIKE :email' .
                        ' LIMIT 20' .
                        ' OFFSET :de',
                    array(
                        ':nome' => '%' . $f3->get('GET.q') . '%',
                        ':email' => '%' . $f3->get('GET.q') . '%',
                        ':de' => $f3->get('GET.de'),

                    )
                )
            );
        }

        echo \Template::instance()->render('templates/admin-novos-cadastros.html');
    }
);

// Página de Administração :: Analisar novos cadastros
$f3->route(
    'GET /admin-novos-cadastros-analisar',
    function ($f3) {
        $f3->set('page', 'admin-novos-cadastros-analisar');

        // Verifica se está logado
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Verifica se tem autorização de acesso
        Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

        $f3->set(
            'res_usuarios',
            $f3->get('db')->exec(
                'SELECT id,nome,nomedamae,email,celular,nacionalidade,naturalidade,genero,cor,' .
                    'datanasc,escolaridade,logradouro,complemento,bairro,cep,pais,estado,cidade,' .
                    'termosdeciencia,permissoes,docIdentificacao,data_cadastro FROM usuarios' .
                    ' WHERE permissoes = "|novo" AND usuarios.id = ?',
                $f3->get('GET.id')
            )
        );

        $f3->set('HTMLtermos', '
            <td>
            <div class="form-check">
            <input class="form-check-input" type="checkbox" value="termos1" name="inputTermos[]" id="inputTermos1" checked disabled>
            <label class="form-check-label" for="inputTermos1">
                As informações acima preenchidas por mim são verdadeiras;
            </label>
            </div><br />
            <div class="form-check">
            <input class="form-check-input" type="checkbox" value="termos2" name="inputTermos[]" id="inputTermos2" checked disabled>
            <label class="form-check-label" for="inputTermos2">
                Os dados inseridos no cadastro são para uso interno da instituição, compreendendo correspondência e prestação de outros serviços que venham a ser solicitados pelo pesquisador cadastrado;
            </label>
            </div><br />
            <div class="form-check">
            <input class="form-check-input" type="checkbox" value="termos3" name="inputTermos[]" id="inputTermos3" checked disabled>
            <label class="form-check-label" for="inputTermos3">
                A consulta aos registros ou a reprodução dos documentos associados não confere propriedade intelectual sobre os mesmos;
            </label>
            </div><br />
            <div class="form-check">
            <input class="form-check-input" type="checkbox" value="termos4" name="inputTermos[]" id="inputTermos4" checked disabled>
            <label class="form-check-label" for="inputTermos4">
             As informações contidas nos documentos consultados ou reproduzidos que vier a divulgar serão obrigatoriamente referenciadas com nome da instituição custodiadora (Arquivo Público do Estado da Bahia / Fundação Pedro Calmon) e de elementos básicos, considerados mínimos como o nome do fundo ou coleção e respectivo código de referência do registro ou respectivo documento;
            </label>
            </div><br />
            <div class="form-check">
            <input class="form-check-input" type="checkbox" value="termos5" name="inputTermos[]" id="inputTermos5" checked disabled>
            <label class="form-check-label" for="inputTermos5">
                Reproduções de documentos de melhor qualidade deve ser objeto de solicitação por meio de requisição de serviço;
            </label>
            </div><br />
            <div class="form-check">
            <input class="form-check-input" type="checkbox" value="termos6" name="inputTermos[]"  id="inputTermos6" checked disabled>
            <label class="form-check-label" for="inputTermos6">
                A divulgação de cópia de documento custodiado, que não seja para fins exclusivamente pessoais, deve ser autorizadas pelo Arquivo Público do Estado da Bahia / Fundação Pedro Calmon.
            </label>
            </div>
        </td>

        ');

        echo \Template::instance()->render('templates/admin-novos-cadastros-analisar.html');
    }
);

// Abrir Documento de Identificacao - Novos cadastros
$f3->route(
    'GET /abrir-docidentificacao-cadastro-novo',
    function ($f3) {
        $f3->set('page', 'admin-novos-cadastros-analisar');

        // Verify if is logged
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Verifica se tem autorização de acesso
        Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

        // Atualiza contagem de acessos
        // $f3->get('db')->exec('UPDATE recortes_reitoria SET img2_acessos = img2_acessos+1 WHERE img2_hash = "' . $f3->get('PARAMS.hash').'"');

        $doc = $f3->get('db')->exec('SELECT docIdentificacao FROM usuarios WHERE id = ?', $f3->get('GET.id'));

        $web = \Web::instance();
        $web->send($f3->get('UPLOADS') . '/identificacao_usuarios/' . $doc[0]['docIdentificacao'], NULL, 512, FALSE);
    }
);

// Aprovar novo cadastro
$f3->route(
    'GET /admin-novos-cadastros-aprovar',
    function ($f3) {
        $f3->set('page', 'admin-novos-cadastros-analisar');

        // Verify if is logged
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Verifica se tem autorização de acesso
        Autorizacao::verificar($f3->get('db'), $f3->get('SESSION.id'), $f3->get('page'));

        // Aprovar
        $f3->get('db')->exec(
            'UPDATE usuarios SET ' .
                'permissoes = :permissoes' .
                ' WHERE id = :id',
            array(
                ':permissoes' => '',
                ':id' => $f3->get('GET.id')
            )
        );

        $f3->set('esteUsuario', $f3->get('db')->exec('SELECT nome,email FROM usuarios WHERE id = ?', $f3->get('GET.id')));

        // Para enviar mensagem por email
        $f3->get('smtp')->set('To', '"' . $f3->get('esteUsuario')[0]['nome'] . '" <' . $f3->get('esteUsuario')[0]['email'] . '>');
        $f3->get('smtp')->set('From', '"' . $f3->get('FARINHA_NOME') . '" <' . $f3->get('FARINHA_EMAIL_LOGIN') . '>');
        $f3->get('smtp')->set('Subject', '[' . $f3->get('FARINHA_NOME') . '] Cadastro efetuado com sucesso');
        $f3->get('smtp')->set('Errors-to', '<' . $f3->get('FARINHA_EMAIL_LOGIN') . '>');
        $f3->get('smtp')->set('content-type', 'text/html;charset=utf-8');
        $f3->set('mensagem', 'Olá' . ' ' . $f3->get('esteUsuario')[0]['nome'] . '!' .
            '<p><strong>Seu cadastro foi aprovado</strong> por nossa equipe. Você já pode utilizar a ' . $f3->get('FARINHA_NOME') . '.</p>' .
            '<p>Para obter sua senha, acesse a página <strong>Recuperar senha</strong> na página de login da Plataforma.</p>' .
            '<p>Atenciosamente,</p>' .
            '<p>' . $f3->get('FARINHA_NOME') . '<br><a href="' . $f3->get('FARINHA_URL') . '">' . $f3->get('FARINHA_URL') . '</a></p>');
        $f3->set('sucessoemail', $f3->get('smtp')->send($f3->get('mensagem')));

        echo '<script>alert("Cadastro aprovado com sucesso!");window.location.href = "/admin-novos-cadastros";</script>';
    }
);

// ====================================================
// Painel - Meu cadastro
// ====================================================

// Meu cadastro
$f3->route(
    'GET /meu-cadastro',
    function ($f3) {
        $f3->set('page', 'meu-cadastro');

        // Verifica se está logado
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        $f3->set(
            'res_usuarios',
            $f3->get('db')->exec(
                'SELECT nome,nomedamae,email,celular,nacionalidade,naturalidade,genero,cor,' .
                    'datanasc,escolaridade,logradouro,complemento,bairro,cep,pais,estado,cidade,' .
                    'termosdeciencia,permissoes,docIdentificacao,data_cadastro FROM usuarios' .
                    ' WHERE usuarios.id = ?',
                $f3->get('SESSION.id')[0]['id']
            )
        );

        $f3->set('HTMLtermos', '
            <td>
            <div class="form-check">
            <input class="form-check-input" type="checkbox" value="termos1" name="inputTermos[]" id="inputTermos1" ' . Autorizacao::termos($f3->get('db'), $f3->get('SESSION.id')[0]['id'], 'termos1') . '>
            <label class="form-check-label" for="inputTermos1">
                As informações acima preenchidas por mim são verdadeiras;
            </label>
            </div><br />
            <div class="form-check">
            <input class="form-check-input" type="checkbox" value="termos2" name="inputTermos[]" id="inputTermos2" ' . Autorizacao::termos($f3->get('db'), $f3->get('SESSION.id')[0]['id'], 'termos2') . '>
            <label class="form-check-label" for="inputTermos2">
                Os dados inseridos no cadastro são para uso interno da instituição, compreendendo correspondência e prestação de outros serviços que venham a ser solicitados pelo pesquisador cadastrado;
            </label>
            </div><br />
            <div class="form-check">
            <input class="form-check-input" type="checkbox" value="termos3" name="inputTermos[]" id="inputTermos3" ' . Autorizacao::termos($f3->get('db'), $f3->get('SESSION.id')[0]['id'], 'termos3') . '>
            <label class="form-check-label" for="inputTermos3">
                A consulta aos registros ou a reprodução dos documentos associados não confere propriedade intelectual sobre os mesmos;
            </label>
            </div><br />
            <div class="form-check">
            <input class="form-check-input" type="checkbox" value="termos4" name="inputTermos[]" id="inputTermos4" ' . Autorizacao::termos($f3->get('db'), $f3->get('SESSION.id')[0]['id'], 'termos4') . '>
            <label class="form-check-label" for="inputTermos4">
                As informações contidas nos documentos consultados ou reproduzidos que vier a divulgar serão obrigatoriamente referenciadas com nome da instituição custodiadora Arquivo XXXXXXXXXXXXXXXXXXXX e de elementos básicos, considerados mínimos como o nome do fundo ou coleção e respectivo código de referência do registro ou respectivo documento;
            </label>
            </div><br />
            <div class="form-check">
            <input class="form-check-input" type="checkbox" value="termos5" name="inputTermos[]" id="inputTermos5" ' . Autorizacao::termos($f3->get('db'), $f3->get('SESSION.id')[0]['id'], 'termos5') . '>
            <label class="form-check-label" for="inputTermos5">
                Reproduções de documentos de melhor qualidade deve ser objeto de solicitação por meio de requisição de serviço;
            </label>
            </div><br />
            <div class="form-check">
            <input class="form-check-input" type="checkbox" value="termos6" name="inputTermos[]"  id="inputTermos6" ' . Autorizacao::termos($f3->get('db'), $f3->get('SESSION.id')[0]['id'], 'termos6') . '>
            <label class="form-check-label" for="inputTermos6">
                A divulgação de cópia de documento custodiado, que não seja para fins exclusivamente pessoais, deve ser autorizadas pelo Arquivo XXXXXXXXXXXX.
            </label>
            </div>
        </td>

        ');

        echo \Template::instance()->render('templates/meu-cadastro.html');
    }
);

// Proc Meu cadastro
$f3->route(
    'POST /proc-meu-cadastro',
    function ($f3) {
        $f3->set('page', 'meu-cadastro');

        // Verifica se está logado
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        if (!empty($_FILES['inputIdentificacao']['tmp_name'])) {
            // UPLOAD
            // Salvar arquivo enviado, se for enviado
            //====
            // Pasta onde o arquivo vai ser salvo
            $_UP['pasta'] = $f3->get('UPLOADS') . '/identificacao_usuarios/';

            // Tamanho máximo do arquivo (em Bytes)
            $_UP['tamanho'] = 1024 * 1024 * 4; // 4Mb

            // Array com as extensões permitidas
            $_UP['extensoes'] = array('jpg', 'png', 'pdf');

            // Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
            $_UP['renomeia'] = true;

            // Array com os tipos de erros de upload do PHP
            $_UP['erros'][0] = 'Não houve erro';
            $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
            $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especificado';
            $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
            $_UP['erros'][4] = 'Não foi feito o upload do arquivo';

            // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
            if ($_FILES['inputIdentificacao']['error'] != 0) {
                die("Não foi possível fazer o upload, erro:<br />" . $_UP['erros'][$_FILES['inputIdentificacao']['error']]);
                exit; // Para a execução do script
            }

            // Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar

            // Faz a verificação da extensão do arquivo
            $extensao = strtolower(end(explode('.', $_FILES['inputIdentificacao']['name'])));
            if (array_search($extensao, $_UP['extensoes']) === false) {
                echo "Por favor, envie arquivos com as seguintes extensões: .jpg, .png ou .pdf";
            }

            // Faz a verificação do tamanho do arquivo
            else if ($_UP['tamanho'] < $_FILES['inputIdentificacao']['size']) {
                echo "O arquivo enviado é muito grande, envie arquivos de até 4 MB.";
            }

            // O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta
            else {
                // Primeiro verifica se deve trocar o nome do arquivo
                if ($_UP['renomeia'] == true) {
                    // Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .csv
                    $nome_final = $f3->get('SESSION.id')[0]['id'] . '.' . $extensao;
                } else {
                    // Mantém o nome original do arquivo
                    $nome_final = $_FILES['inputIdentificacao']['name'];
                }

                // Depois verifica se é possível mover o arquivo para a pasta escolhida
                if (move_uploaded_file($_FILES['inputIdentificacao']['tmp_name'], $_UP['pasta'] . $nome_final)) {
                    // Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
                    //echo "Upload efetuado com sucesso!";
                    //echo '<br /><a href="' . $_UP['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
                } else {
                    // Não foi possível fazer o upload, provavelmente a pasta está incorreta
                    //echo "Não foi possível enviar o arquivo, tente novamente";
                }
            }
        } else {
            $nome_final = $f3->get('POST.inputIdentificacao');
        }
        // ====

        // Termos de ciência
        $termos = $f3->get('POST.inputTermos');
        if ($termos != '') {
            $termo = implode('|', $termos);
        }
        if ($termos == '') {
            $termo = '';
        }
        if ($termos == 'termos1|termos2|termos3|termos4|termos5|termos6') {
            // Se o usuário já aceitou todos os termos antes, apenas mantém
            $termo = 'termos1|termos2|termos3|termos4|termos5|termos6';
        }

        // Processar dados do formulário
        $f3->get('db')->exec(
            'UPDATE usuarios SET ' .
                'nome = :nome, ' .
                'nomedamae = :nomedamae, ' .
                'email = :email, ' .
                'celular = :celular, ' .
                'nacionalidade = :nacionalidade, ' .
                'naturalidade = :naturalidade, ' .
                'genero = :genero, ' .
                'cor = :cor, ' .
                'datanasc = :datanasc, ' .
                'escolaridade = :escolaridade, ' .
                'logradouro = :logradouro, ' .
                'complemento = :complemento, ' .
                'bairro = :bairro, ' .
                'cep = :cep, ' .
                'pais = :pais, ' .
                'estado = :estado, ' .
                'cidade = :cidade, ' .
                'termosdeciencia = :termosdeciencia, ' .
                'docIdentificacao = :docIdentificacao ' .
                'WHERE id = :id',
            array(
                ':nome' => $f3->get('POST.inputNome'),
                ':nomedamae' => $f3->get('POST.inputNomeDaMae'),
                ':email' => $f3->get('POST.inputEmail'),
                ':celular' => $f3->get('POST.inputCelular'),
                ':nacionalidade' => $f3->get('POST.inputNacionalidade'),
                ':naturalidade' => $f3->get('POST.inputNaturalidade'),
                ':genero' => $f3->get('POST.inputGenero'),
                ':cor' => $f3->get('POST.inputCor'),
                ':datanasc' => $f3->get('POST.inputDataNasc'),
                ':escolaridade' => $f3->get('POST.inputEscolaridade'),
                ':logradouro' => $f3->get('POST.inputLogradouro'),
                ':complemento' => $f3->get('POST.inputComplemento'),
                ':bairro' => $f3->get('POST.inputBairro'),
                ':cep' => $f3->get('POST.inputCEP'),
                ':pais' => $f3->get('POST.inputPais'),
                ':estado' => $f3->get('POST.inputEstado'),
                ':cidade' => $f3->get('POST.inputCidade'),
                ':termosdeciencia' => $termo,
                ':docIdentificacao' => $nome_final,
                ':id' => $f3->get('SESSION.id')[0]['id']
            )
        );

        echo '<script>alert("As modificações foram salvas!");history.back();</script>';
    }
);

// Ajuda
$f3->route(
    'GET /ajuda',
    function ($f3) {
        $f3->set('page', 'ajuda');

        // Verifica se está logado
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        echo \Template::instance()->render('templates/ajuda.html');
    }
);

// Proc Ajuda
$f3->route(
    'POST /proc-ajuda',
    function ($f3) {
        $f3->set('page', 'ajuda');

        // Verifica se está logado
        if (!$f3->get('SESSION.logged')) {
            $f3->reroute('/login');
        }

        // Para enviar mensagem por email
        $f3->get('smtp')->set('To', '"Fulano" <Fulano@exemplo.com>');
        $f3->get('smtp')->set('From', '"' . $f3->get('FARINHA_NOME') . '" <' . $f3->get('FARINHA_EMAIL_LOGIN') . '>');
        $f3->get('smtp')->set('Reply-To', '"' . $f3->get('SESSION.nome')[0]['nome'] . '" <' . $f3->get('SESSION.email') . '>');
        $f3->get('smtp')->set('Subject', '[' . $f3->get('FARINHA_NOME') . '] ' .  $f3->get('POST.inputAssunto'));
        $f3->get('smtp')->set('Errors-to', '<Fulano@exemplo.com>');
        $f3->get('smtp')->set('content-type', 'text/html;charset=utf-8');
        $f3->set('mensagem', '<p><i>Mensagem enviada pela Ajuda do sistema</i></p>========<br><p>' . nl2br(($f3->get('POST.inputTexto'))) . '</p>');
        $f3->set('sucessoemail', $f3->get('smtp')->send($f3->get('mensagem')));

        echo '<script>alert("Obrigado pelo contato! Aguarde nossa resposta por email. Tentaremos ser breves!");history.back();</script>';
    }
);

// Adiciona código customizado na Farinha
require('customizacao.inc.php');

$f3->run();
